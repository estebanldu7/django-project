from django.shortcuts import render
from django.http import HttpResponse
from orderManage.models import Item
from django.core.mail import send_mail
from django.conf import settings
from orderManage.forms import ContactForm


# Create your views here.
def search_products(request):
    return render(request, "search_products.html")


def search(request):
    if request.GET["product"]:
        product = request.GET["product"]
        if len(product) > 20:
            message = "Texto de busqueda demasiado largo"
        else:
            items = Item.objects.filter(name__icontains=product)  # name like ''
            return render(request, 'result_searched.html', {"items": items, "query": product})
    else:
        message = "Ingrese un parametro a buscar"

    return HttpResponse(message)


def contact(request):
    if request.method == 'POST':
        contactForm = ContactForm(request.POST)

        if contactForm.is_valid():
            information = contactForm.cleaned_data
            send_mail(
                information['subject'],
                information['message'],
                information.get('email', 'noreply@example.com'),
                ['epreciado@linux.com'],
            )

            return render(request, "thanks.html")
    else:
        contactForm = ContactForm()
        
    return render(request, "form_contact.html", {"form": contactForm})

