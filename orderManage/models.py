from django.db import models

# Create your models here.

class Client(models.Model):
    name = models.CharField(max_length=30, verbose_name='Nombre')
    address = models.CharField(max_length=255, verbose_name='Direccion')
    email = models.EmailField(blank=True, null=True, verbose_name='Email')
    phone = models.CharField(max_length=13, verbose_name='Telefono')

    def __str__(self):
        return self.name

class Item(models.Model):
    name = models.CharField(max_length=30, verbose_name='Nombre')
    section = models.CharField(max_length=30, verbose_name='Seccion')
    price = models.IntegerField(verbose_name='Precio')

    def __str__(self):
        return self.name

class Order(models.Model):
    number = models.IntegerField(default=True)
    date = models.DateField(blank=True, null=True)
    delivery = models.BooleanField()

#run this commands for create tables in sql engine:
#python3 manage.py makemigrations
#python3 manage.py sqlmigrate orderManage MIGRATION NUMBER
#python3 manage.py migrate
