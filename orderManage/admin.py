from django.contrib import admin
from orderManage.models import Client, Item, Order

# Register your models here.
class ClientAdmin(admin.ModelAdmin):
    fields = ('name', 'address', 'phone')
    list_display = ('name', 'phone')
    list_filter = ('name', 'phone')
    search_fields = ('name', 'phone')

class ItemAdmin(admin.ModelAdmin):
    list_filter = ('name', 'section')
    search_fields = ('name', 'section')

class OrderAdmin(admin.ModelAdmin):
    fields = ('number', 'date')
    list_display = ('number', 'date')
    date_hierarchy = ('date')

admin.site.register(Client, ClientAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Order, OrderAdmin)
